describe('login', function() {

    beforeEach(() => {
        cy.fixture("/users").as("gitUser");
        cy.visit('/')
    })

    it('it greets with Sign up for GitHub page', function() {

        cy.contains("Sign up for GitHub")

        })

    it('it greets with Sign in to GitHub', function() {
        cy.get ('[href="/login"]').click({ force: true });
        cy.contains("Sign in to GitHub")
    })

    it('it requires valid email and password ',function () {
        cy.get ('[href="/login"]').click({ force: true });
        cy.get ('[name="login"]').type(this.gitUser.name);
        cy.get ('[name="password"]').type('Invalid');
        cy.get ('[name="commit"]').click();
        cy.contains("Incorrect username or password.").should('be.visible')
    })

    it('login successsfully ',function () {
        cy.get ('[href="/login"]').click({ force: true });
        cy.get ('[name="login"]').type(this.gitUser.name);
        cy.get ('[name="password"]').type(this.gitUser.password);
        cy.get ('[name="commit"]').click();
        cy.contains('Learn Git and GitHub without any code!').should('be.visible')
    })



})


