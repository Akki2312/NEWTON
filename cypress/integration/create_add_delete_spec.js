describe('createRepository_addReadme_deleteRepository', function() {
    beforeEach(function() {
        cy.login();
        cy.fixture("/repo").as("details");
        cy.fixture("/users").as("gitUser");

    })

    it('Create repository', function() {

        cy.get('a.btn.btn-sm').click();
        cy.get ('[id="repository_name"]').type(this.details.reponame);
        cy.get('button.btn.btn-primary').click();
        cy.contains(this.details.reponame).should('be.visible')


        })

    it('Add read me file', function() {
        cy.contains(this.details.reponame).click();
        cy.get('[data-ga-click=\'Empty repo, click, Clicked README link\']').click();
        cy.get ('[id="commit-summary-input"]').type(this.details.readmename);
        cy.get('button#submit-file').click()
    })


    it('Delete repository', function() {
          cy.contains(this.details.reponame).click();
          var user = this.gitUser.name;
          var repo = this.details.reponame;
          var comb = "/"+user+"/"+repo+"/settings";
          var setting = '[href="'+comb+'"]';
          var del = '"'+user+"/"+repo+'"';

          cy.get(setting).click();
          cy.get('[aria-haspopup="dialog"]').click();
          cy.get('[aria-label="Type in the name of the repository to confirm that you want to delete this repository."]').type(this.details.reponame);
          cy.contains("I understand the consequences, delete this repository").click()
          cy.contains("Your repository "+del+" was successfully deleted.")


    })


});
